	
	// this are Lines/block of codes that tell our device/application to perform a certain task when called/ invoked
	// function are mostly created to create complicated tasks to run several lines of code in succession
	// they are also used to prevent repeating lines /block of codes that perform the same task/functoion

	// function declarations.
		// (function statement) definces a function w/ the specified parameters

			/*
				syntax:
				function functionname(){
	
				}
			*/
			// function keyword- used to defince JS function
			// functionName- the function name functions are named to be able to use later in coded
			// function block({}) - the statement w/c comprise the vody of the function .this is where the code is to be executed
			// we cab assign a variable to hold a function, but will be e

	function printName (argument) {

		console.log('ralph Dagasdas')
	};
	printName();

	// semicolon are used to separated excutable JS statements.

	// function Invocation
		// the code block and statement inside a function is not immediately excuted when the function is defined. the code block and statements inside a function is executed when the function is invoked or called
		// it it common to use the term "call the function" instead of "invoke a function"

			printName();
				//declaredfunction();//error// this is not defined

	// function Declaration Vs expressions
		// function declarations
			// a function can be created through function declaration by using the function keyword and adding function name
			// declared function are not executed immediately. they are saved for later use', and will be excuted  later, when they are invoked(called upon)

			declaredFunction();

			function declaredFunction(){
				console.log("hellow from declaredFunction")
			}


	// function expression
		// a function can also be stored in a variable. this is called function expression
		// a function expression is an anonymous function is assigned to the variable function 
		// anonymous function- function w.out a name

		// variableFunction();// this will be error.
		/*
			error- function expression , being stored in a let or const varuable cannot be hoisted
		*/
	let variableFunction = function(){
		console.log(" hello again")
	}

	variableFunction();


	//we can also created a function expression of a named function
	// however, to invoke the function expression , we invoke it by its variable name, not function name

	let functionExpression =function funcName(){
		console.log('hellow from other planet')
	}

		// funcName();
		functionExpression();


		declaredFunction =function (){
			console.log("updated declaredFunction")
		};

		declaredFunction();

		functionExpression =function (){
			console.log('updated functionExpression')
		}

		functionExpression();

		//  we connot reassign a function expression initialized w/ const
		const constantfunc =function(){
			console.log("Initialized with const")
		}

		constantfunc();

		// 	constantfunc = function (){
		// 		console.log('connot be re-assigned')
		// 	}

		// constantfunc(); this is error connot reassigned

	// function scoping
	/*
		scope is the accesibility(visibility) of variable w/in our program
		js variable has 3 type of scope:
		1. local/ block scope
		2. global scope
		3. function scope

	*/
	// let globalVar= "mr. ww"
	// console.log(globalVar)

	{
		let localVar = "armando perez"
		console.log(localVar)
		// console.log(globalVar)// error dint initialized inside the block
			// we connot invoked a globalVar inside a block
			// can only access if declared before the code block
	}
	let globalVar= "mr. ww"
	console.log(globalVar)
	// console.log(localVar) // error
	//localVar being inside a block cant be accessed outside of its block of code


	// function scope
	/*
		JS has a function scope: each function created a new scope variable defined inside a function are not accessible (visible ) from outside
		variable declered w/ var, let, const, are quite similet when declared insde a function
	*/

	function showName() {
		//fucntion scoped variables
		var functionVar = 'joe'
		const functionConst='jhon'
		let functionLet = "jane"

		console.log(functionVar)
		console.log(functionConst)
		console.log(functionLet)
	}
	showName();

		// console.log(functionVar) error
		// console.log(functionConst) error
		// console.log(functionLet) error

		/*
			the functionVar, functionConst, functionLet are fucntion scoped and connot be accesed outside of the function they were declared in
		 */

	// Nested Function
		//  you can created another function inside a function
		//  this is called a nested function

	function myNewFunction(){
		let name = 'Cee';

			function nestedFunction(){
				let nestedName= 'thor'
				console.log(name)
				console.log(nestedName)
			};
			//console.log(nestedName)
			// result error
			// nestedName is not defined
			// nestedName variable , being declared int h nestedFunction connot be accesed outside of the function it was declared in

			nestedFunction();
	};
	myNewFunction();
	// nestedFunction();
	/*
		since this function is declated inside myNewFunction, it too cannot be invoked outside of the function it was declared in
	*/

	// function and global scoped Variables

	//  this is a global scope variable
	let globalName = "Nej";

	 function myNewfunction2(){
	 	let nameInside = 'martin';
	 	// variable declared globally (outside any function ) have global scope
	 	// global variable can be accessed from anywhere in JS program including from inside a function
	 	
	 	console.log(globalName);
	 };

	 myNewfunction2 ();


	// using alert()
		//  alert allows us to show small window at the top of brower
		// as opposed to a console.log w/c will only show in console
		//  it allows us to show a short dialog or instruction to our user

		// alert('hello teddy')

		function sampleAlert(){
			alert('hellow inday')

		};
		

		console.log(" i will only show if aller ist ")



