/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function userProfile(){
			let fullname = prompt('Enter your fullname');
			let age = prompt('how young are you');
			let location = prompt('where do you live');

			console.log('My name is ' + fullname)
			console.log('Im '+ age + ' years old')
			console.log('You live in ' +location)
	}

	userProfile();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favBand(){
			let topBand1 = 'Queen';
			let topBand2 = 'metallica';
			let topBand3 = 'Parokya ni Edgar'; 72
			let topBand4 = 'South Border ';
			let topBand5 = 'siakol';

		console.log('My top 5 Band:')
		console.log('1.' + topBand1)
		console.log('2.' + topBand2)
		console.log('3.' + topBand3)
		console.log('4.' + topBand4)
		console.log('5.' + topBand5)

	}

	favBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favMovies(){
			let Movie1 = 'John Wick'; 86
			let Movie2 = 'Iron Man';94
			let Movie3 = 'Pacific Rim';72
			let Movie4 = 'CAPTAIN AMERICA: THE FIRST AVENGER';79
			let Movie5 = 'Batman Begins';84

		
		console.log('Rotten Tomatoes Rating: 82%'+"\n" +'1.' + Movie1)
		console.log('Rotten Tomatoes Rating: 94%'+"\n" +'2.' + Movie2)
		console.log('Rotten Tomatoes Rating: 72%'+"\n" +'3.' + Movie3)
		console.log('Rotten Tomatoes Rating: 79%'+"\n" +'4.' + Movie4)
		console.log('Rotten Tomatoes Rating: 84%'+"\n" +'5.' + Movie5)

	}

	favMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


	function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printUsers();


// console.log(friend1);
// console.log(friend2);